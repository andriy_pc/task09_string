package main.functions;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Find word with the given length in questions sentences
 */

public class WordsInQuestionSent extends Function {
    private static Set<String> words = new HashSet<String>();
    private static List<String> sentences = new LinkedList<>();

    public void execute(String text) {
        System.out.println("Please insert length");
        Scanner in = new Scanner(System.in);
        execute(text, in.nextInt());
    }

    public void execute(String text, int length) {
        text = text.trim();
        Stream<String> in = Stream.of(text);
        words = in.flatMap(s -> Stream.of(s.split("\n")))
                .peek(s -> { s = s.trim(); })
                .filter(s -> s.endsWith("?"))
                .flatMap(s -> Stream.of(s.split("\\W")))
                .peek(s -> {})
                .filter(s -> s.length() == length)
                .collect(Collectors.toSet());
        System.out.println(words);
    }
}
