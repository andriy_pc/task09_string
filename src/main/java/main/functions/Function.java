package main.functions;

public abstract class Function {
    public abstract void execute(String text);
}
