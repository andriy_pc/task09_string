package main.functions;

import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Слова тексту, що починаються з голосних букв, відсортувати в
 * алфавітному порядку по першій приголосній букві слова.
 */

public class SortVowel extends Function {
    private List<String> vowelWords;

    public void execute(String text) {
        Pattern vowel = Pattern.compile("^[aeouiAEOUI]");
        Stream.of(text)
                .flatMap(s -> Stream.of(s.split("[.] |[.]|\n")))
                .peek(s -> {})
                .flatMap(s -> Stream.of(s.toLowerCase().split("\\W+")))
                .filter(s -> vowel.matcher(s).find())
                .sorted(new MyCmp())
                .forEach(s -> System.out.println(s));
    }
}

class MyCmp implements Comparator<String> {
    Pattern unVowel = Pattern.compile("[^aeouiAEOUI]");

    public int compare(String s1, String s2) {
        Matcher m1 = unVowel.matcher(s1);
        Matcher m2 = unVowel.matcher(s2);
        m1.find();
        m2.find();
        int c1 = m1.start();
        int c2 = m2.start();
        return s1.charAt(m1.start()) - s2.charAt(m2.start());
    }
}
