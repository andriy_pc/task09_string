package main.functions;

import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * Sort words with percent of their vowel letters
 */

public class PercentOfVowel extends Function {
    private static Map<String, Float> percentOfVowel = new TreeMap<>();

    public void execute(String text) {
        text = text.trim();
        Stream<String> in = Stream.of(text);
        in.flatMap(s -> Stream.of(s.split("[.] |[.]|\n")))
                .peek(s -> { s = s.trim(); })
                .flatMap(s -> Stream.of(s.split("\\W+")))
                .forEach(s -> {
                    percentOfVowel.put(s, (float)countVowel(s)/s.length());
                });
        display();
    }
    private static long countVowel(String word) {
        Pattern vowelPattern = Pattern.compile("[aeouiAEOUI]");
        long vowel = 0;
        vowel = word.chars()
                .mapToObj(c -> (char) c+"")
                .peek(c -> {})
                .filter(c -> vowelPattern.matcher(c).find())
                .count();
        return vowel;
    }

    public static void display() {
        Map<String, Float> t = WordGrowthOrder.sortByValue(percentOfVowel);
        for(String s : t.keySet()) {
            System.out.println(s+": "+t.get(s));
        }
    }
}
