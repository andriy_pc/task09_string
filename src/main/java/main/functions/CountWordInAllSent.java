package main.functions;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Є текст і список слів. Для кожного слова з заданого списку знайти, скільки
 * разів воно зустрічається у кожному реченні, і відсортувати слова за
 * спаданням загальної кількості входжень
 */

public class CountWordInAllSent extends Function {
    Set<String> wordsToCount;
    Map<String, Integer> wordValue;

    public void execute(String text) {
        init();
        wordValue = new LinkedHashMap<>();
        Stream.of(text)
                .flatMap(s -> Stream.of(s.split("[.] |[.]|\n")))
                .flatMap(s -> Stream.of(s.split("\\W+")))
                .flatMap(s -> Stream.of(s.toLowerCase()))
                .filter(s -> wordsToCount.contains(s))
                .forEach(s -> {
                    wordValue.computeIfAbsent(s, v -> 0);
                    wordValue.merge(s, 1, (oldV, newV) -> oldV + newV);
                });
        wordValue = sortByValue(wordValue);
        display();
    }

    private void display() {
        for(String s : wordValue.keySet()) {
            System.out.println(s + ": " + wordValue.get(s));
            wordsToCount.remove(s);
        }
        for(String s : wordsToCount) {
            System.out.println(s + ": " + 0);
        }
    }

    private static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
        list.sort(Map.Entry.comparingByValue((i1, i2) -> i2.compareTo(i1)));

        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

    private void init() {
        System.out.println("Please input words you want to count");
        System.out.println("(Insert all in one line!)");
        Scanner in = new Scanner(System.in);
        String tmp = in.nextLine();
        wordsToCount = Stream.of(tmp)
                .flatMap(s -> Stream.of(s.split("\\W+")))
                .flatMap(s -> Stream.of(s.toLowerCase()))
                .collect(Collectors.toSet());
    }
}
