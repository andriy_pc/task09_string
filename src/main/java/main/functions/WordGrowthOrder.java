package main.functions;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Print sentences in order of growing quantity of words
 */

public class WordGrowthOrder extends Function {
    private static Map<Integer, Long> wordsQuantity = new LinkedHashMap<>();
    private static List<String> sentences = new ArrayList<>();

    public void execute(String text) {
        text = text.trim();
        Stream<String> in = Stream.of(text);
        sentences = in.flatMap(s -> Stream.of(s.split("[.] |[.]|\n")))
                .peek(s -> {s = s.trim();})
                .collect(Collectors.toList());
        for(int i = 0; i < sentences.size(); i++) {
            Stream<String> sent = Stream.of(sentences.get(i));
            long counter = sent
                    .flatMap(s -> Stream.of(s.split("\\W+")))
                    .peek(s -> {})
                    .filter(s -> !s.matches("\\W"))
                    .count();
            wordsQuantity.put(i, counter);
        }
        wordsQuantity = sortByValue(wordsQuantity);
        display();
    }

    static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
        list.sort(Map.Entry.comparingByValue());

        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }
    public static void display() {
        for(int i : wordsQuantity.keySet()) {
            System.out.println(sentences.get(i));
        }
    }
    /*public static void main(String[] args) {
        execute("A way to start doing this is to ask. " +
                "If I could magically pull them out of a hat, what objects would solve my problem right away. " +
                "For example, suppose you are creating a bookkeeping program. " +
                "You might imagine some objects that contain pre-defined bookkeeping input screens, " +
                "another set of objects that perform bookkeeping calculations, and an object that handles " +
                "printing of checks and invoices on all different kinds of printers. " +
                "Maybe some of these objects already exist, and for the ones that don’t, what would they look like.");
    }*/
}
