package main.functions;

import java.util.*;
import java.util.stream.Stream;

/**
 * Print words in alphabetical order comparing only first letter
 * Each new letter word has to start with \t
 */

public class AlphabeticalOrder extends Function {
    private static Map<Character, List<String>> alphabeticOrder = new TreeMap<>();

    public void execute(String text) {
        text = text.trim();
        Stream<String> in = Stream.of(text);
        in.flatMap(s -> Stream.of(s.split("[.] |[.]|\n")))
                .peek(s -> { s = s.trim(); })
                .flatMap(s -> Stream.of(s.split("\\W+")))
                .forEach(s -> {
                    alphabeticOrder.computeIfAbsent(s.toLowerCase().charAt(0), value -> new ArrayList<String>()).add(s);
                });
        display();
    }

    public static void display() {
        for(char c : alphabeticOrder.keySet()) {
            StringBuilder result = new StringBuilder("\t");
            for(String s : alphabeticOrder.get(c)) {
                result.append(s.toLowerCase());
                result.append(", ");
            }
            System.out.println(result.toString());
        }
        System.out.println();
    }


    /*public static void main(String[] args) {
        execute("A way to start doing this is to ask. " +
                "If I could magically pull them out of a hat, what objects would solve my problem right away. " +
                "For example, suppose you are creating a bookkeeping program. " +
                "You might imagine some objects that contain pre-defined bookkeeping input screens, " +
                "another set of objects that perform bookkeeping calculations, and an object that handles " +
                "printing of checks and invoices on all different kinds of printers. " +
                "Maybe some of these objects already exist, and for the ones that don’t, what would they look like.");
    }*/
}
