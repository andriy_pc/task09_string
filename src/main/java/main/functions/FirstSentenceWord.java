package main.functions;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//Have to make it work properly
//It can't use indexOf() to see if a word is in the sentence

/**
 * Find a word in the first sentence, which is unique for all another sentences.
 */
public class FirstSentenceWord extends Function {
    private static List<String> sentences = new ArrayList<>();
    private static ArrayList<String> uniqueWords = new ArrayList<>();
    private static boolean matches = false;

    public void execute(String text) {
        String[] words;
        text = text.trim();
        Stream<String> in = Stream.of(text);
        sentences = in.flatMap(s -> Stream.of(s.split("[.] |[.]|\n")))
                .peek(s -> {s = s.toLowerCase();})
                .collect(Collectors.toList());
        words = sentences.get(0).split("\\W+");
        for(String s : words) {
            matches = false;
            for(int i = 1; i < sentences.size(); i++) {
                int tmp = sentences.get(i).indexOf(s);
                if( tmp != -1) {
                    matches = true;
                    break;
                } else if (i == sentences.size()-1 && !matches) {
                    uniqueWords.add(s);
                }
            }
        }
        System.out.println(uniqueWords);

    }

    /*public static void main(String[] args) {
        execute("A way to start doing this is to ask. " +
                "If I could magically pull them out of a hat, what objects would solve my problem right away. " +
                "For example, suppose you are creating a bookkeeping program. " +
                "You might imagine some objects that contain pre-defined bookkeeping input screens, " +
                "another set of objects that perform bookkeeping calculations, and an object that handles " +
                "printing of checks and invoices on all different kinds of printers. " +
                "Maybe some of these objects already exist, and for the ones that don’t, what would they look like.");
    }*/

}
