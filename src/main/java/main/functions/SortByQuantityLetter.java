package main.functions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SortByQuantityLetter extends Function {
    private List<String> words;
    private Map<String, Long> wordLetter = new HashMap<>();

    public void execute(String text) {
        Scanner in = new Scanner(System.in);
        System.out.println("Please insert letter");
        String letter = in.next("\\w");
        Pattern letterPattern = Pattern.compile(letter);
        words = Stream.of(text)
                .flatMap(s -> Stream.of(s.split("[.] |[.]|\n")))
                .peek(s -> { s = s.trim(); })
                .flatMap(s -> Stream.of(s.split("\\W+")))
                .collect(Collectors.toList());
        for(String w : words) {
            Long val = w.chars()
                    .mapToObj(c -> (char) c+"")
                    .filter(c -> c.equals(letter))
                    .count();
            System.out.println(w == null);
            wordLetter.put(w, val);
        }
        wordLetter = WordGrowthOrder.sortByValue(wordLetter);
        display();
    }

    public void display() {
        for(String k : wordLetter.keySet()) {
            System.out.println(k + ": " + wordLetter.get(k));
        }
    }

    /*public static void main(String[] args) {
        SortByQuantityLetter s = new SortByQuantityLetter();
        s.execute("Bndrii. Andrii. Cndrii. ");
    }*/
}
