package main.functions;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Switch the longest word with first word which starts with vowel letter
 */

public class SwitchWithTheLongest extends Function {
    private static List<String> sentences = new ArrayList<>();

    public void execute(String text) {
        String[] words;
        text = text.trim();
        Stream<String> in = Stream.of(text);
        sentences = in.flatMap(s -> Stream.of(s.split("[.] |[.]|\n")))
                .peek(s -> {
                    s = s.trim();
                })
                .collect(Collectors.toList());
        switchWords();
        }


    private static void switchWords() {
        boolean vowel = false;
        String vowelWord = "";
        String longestWord = "";
        String tmp = "ta@adsa";

        Pattern p = Pattern.compile("^[aeouiAEOUI].*");
        Matcher m;

        for (String sent : sentences) {
            System.out.println(sent);
            vowel = false;
            int longest = -1;
            for (String word : sent.split("\\W+")) {
                if (word.length() > longest) {
                    longest = word.length();
                    longestWord = word;
                }
                m = p.matcher(word);
                if (m.find() && !vowel) {
                    vowelWord = m.group();
                    vowel = true;
                }
            }
            sent = sent.replaceFirst(vowelWord, tmp);
            sent = sent.replaceAll(longestWord, vowelWord.toUpperCase());
            sent = sent.replaceFirst(tmp, longestWord.toUpperCase());
            System.out.println(sent);
        }
        return;
    }

    /*public static void main(String[] args) {
        execute("A way to start doing this is to ask. " +
                "If I could magically pull them out of a hat, what objects would solve my problem right away. " +
                "For example, suppose you are creating a bookkeeping program. " +
                "You might imagine some objects that contain pre-defined bookkeeping input screens, " +
                "another set of objects that perform bookkeeping calculations, and an object that handles " +
                "printing of checks and invoices on all different kinds of printers. " +
                "Maybe some of these objects already exist, and for the ones that don’t, what would they look like.");
    }*/
}
