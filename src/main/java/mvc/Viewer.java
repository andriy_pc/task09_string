package mvc;

import java.util.*;

public class Viewer {
    private ArrayList<Locale> locales;

    private Map<String, String> menu;
    private ResourceBundle bundle;

    public Viewer() {
        locales = new ArrayList<>(
                Arrays.asList(
                        new Locale("uk", "UA"),
                        new Locale("en", "US"),
                        new Locale("fr", "FR"),
                        new Locale("de", "DE")
                ));
        initLocale(0);
    }

    private void initMenu(Locale loc) {
        menu = new LinkedHashMap<>();
        bundle = ResourceBundle.getBundle("menu", loc);

        menu.put("0", bundle.getString("0"));
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("7", bundle.getString("7"));
        menu.put("8", bundle.getString("8"));
    }

    public void display() {
        System.out.println("Please select the option: \n");
        for(String s : menu.keySet()) {
            System.out.println(menu.get(s));
        }
    }

    public void initLocale(int choice) {
        initMenu(locales.get(choice));
    }
}
