package mvc;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Controller {
    private Model model;
    private Scanner in;
    private Viewer view;
    private Map<Integer, Display> menu;
    private Map<Integer, String> locales;

    public Controller(Model m, Viewer v) {
        model = m;
        view = v;
        in = new Scanner(System.in);
        init();
    }
    public void init() {
        menu = new LinkedHashMap<>();
        menu.put(0, this::quit);
        menu.put(1, this::alphabetOrder);
        menu.put(2, this::firstSentWrd);
        menu.put(3, this::percentOfVowels);
        menu.put(4, this::switchTheLongest);
        menu.put(5, this::growthOrder);
        menu.put(6, this::wordsInQuestions);
        menu.put(7, this::countWordInAllSentences);
        menu.put(8, this::changeLanguage);

        locales = new LinkedHashMap<>();
        locales.put(0, "Ukrainian");
        locales.put(1, "English");
        locales.put(2, "Le français");
        locales.put(3, "Auf Deutsch");
    }

    public void run() throws IOException {
        view.display();
        int choice = in.nextInt();
        menu.get(choice).display();
    }
    public void quit() {
        System.out.println("Quit...");
        System.exit(0);
    }
    public void alphabetOrder() {
        model.perform(0);
    }
    public void firstSentWrd() {
        model.perform(1);
    }
    public void percentOfVowels() {
        model.perform(2);
    }
    public void switchTheLongest() {
        model.perform(3);
    }
    public void growthOrder() {
        model.perform(4);
    }
    public void wordsInQuestions() {
        model.perform(5);
    }
    public void countWordInAllSentences() {
        model.perform(6);
    }
    public void changeLanguage() {
        System.out.println("Please select language:");
        for(Integer i : locales.keySet()) {
            System.out.println(i + ") " + locales.get(i));
        }
        int choice = in.nextInt();
        view.initLocale(choice);
    }
}
