package mvc;

import java.io.IOException;

public class Application {

    public static void main(String[] args) {
        Viewer view = new Viewer();
        Controller controller = new Controller(new Model(view), view);
        try {
            while(true) {
                controller.run();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace(System.err);
        }
    }
}
