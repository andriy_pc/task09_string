package mvc;

import main.functions.*;

import java.util.Map;

public class Model {
    private Function[] functions;
    private String text = "A way to start doing this is to ask. " +
            "If I could magically pull them out of a hat, what objects would solve my problem right away. " +
            "For example, suppose you are creating a bookkeeping program. " +
            "You might imagine some objects that contain pre-defined bookkeeping input screens, " +
            "another set of objects that perform bookkeeping calculations, and an object that handles " +
            "printing of checks and invoices on all different kinds of printers. " +
            "Maybe some of these objects already exist, and for the ones that do not, what would they look like.";
    private Viewer view;

    public Model(Viewer v) {
        view = v;
        functions = new Function[]{
                new AlphabeticalOrder(),
                new FirstSentenceWord(),
                new PercentOfVowel(),
                new SwitchWithTheLongest(),
                new WordGrowthOrder(),
                new WordsInQuestionSent(),
                new CountWordInAllSent()
        };
    }

    public void perform(int i) {
        functions[i].execute(text);
    }
}
